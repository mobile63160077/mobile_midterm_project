import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile_midterm_work/approve_page.dart';
import 'package:mobile_midterm_work/bibliography_page.dart';
import 'package:mobile_midterm_work/check_debt_page.dart';
import 'package:mobile_midterm_work/dept_scholar_page.dart';
import 'package:mobile_midterm_work/enroll_result.dart';
import 'package:mobile_midterm_work/graduate_page.dart';
import 'package:mobile_midterm_work/student_list_page.dart';
import 'package:mobile_midterm_work/study_result_page.dart';
import 'package:mobile_midterm_work/suggest_page.dart';
import 'package:mobile_midterm_work/timetable_page.dart';
import 'package:mobile_midterm_work/web_log_page.dart';
import 'enroll_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: RegTheme.defaultTheme(),
      home: const Home(),
    );
  }
}

class RegTheme {
  static ThemeData defaultTheme() {
    return ThemeData(
        appBarTheme: const AppBarTheme(
            color: Color(0xffffd700), foregroundColor: Colors.black),
        
        dividerTheme: DividerThemeData(color: Colors.red),

        // iconTheme: IconThemeData(color: Colors.grey)
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late final PageController pageController;
  int pageNo  = 0;
  Timer? carouselTimer;
  Timer getTimer(){
    return Timer.periodic(Duration(seconds: 4), (timer) {
      if(pageNo == 5){
        pageNo = 0;
      }
      pageController.animateToPage(pageNo, duration: Duration(seconds: 2), curve: Curves.easeInOutCirc);
      pageNo++;
    });
  }
  final picArray = ["assets/graduatePic.jpg","assets/JobFair.jpg","assets/muayEvent.jpg","assets/courseLook.jpg","assets/graduateApartment.jpg"];

  @override
  void initState() {
    pageController = PageController(
      initialPage: 0,
      viewportFraction: 0.85
    );
    carouselTimer = getTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyNavigationDrawer(),
        appBar: AppBar(
          title: const Text("Home"),
        ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text("Welcome to E-registrar, Burapha University", style: TextStyle(fontSize: 20),),
              decoration: BoxDecoration(
                color: Colors.grey[300]
              )
            ),Container(
              margin: EdgeInsets.only(top: 20),
              child: Text("Events", style: TextStyle(fontSize: 26),),
            ),
            SizedBox(
              height: 200,
              child: PageView.builder(
                controller: pageController,
                onPageChanged: (index){
                  pageNo = index;
                  setState(() {
                  });
                },
                itemBuilder: (_, index) {
                  return AnimatedBuilder(
                    animation: pageController,
                    builder: (ctx, child) {
                      return child!;
                    },
                    child: GestureDetector(
                      onTap: (){
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("card ${index+1}")));
                      },
                      onPanDown: (d){
                        carouselTimer?.cancel();
                        carouselTimer = null;
                      },
                      onPanCancel: (){
                        carouselTimer = getTimer();
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 8,left: 8,top: 24, bottom: 8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(24),
                            color: Colors.grey,
                          image: DecorationImage(image: AssetImage(picArray[index]),fit: BoxFit.cover)
                        ),

                      ),
                    ),
                  );
                },
                itemCount: 5,
              ),
            ),
            SizedBox(height: 6,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(5, (index) => GestureDetector(
                onTap: (){
                  pageNo = index;
                },
                child: Container(
                  margin: EdgeInsets.all(2),
                  child: Icon(
                    Icons.circle,
                    size: 12,
                    color: pageNo == index? Color(0xffffd700): Colors.grey,
                  ),),
              )),),
            Divider(),
            Text('Announcement', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
            Expanded(
              child: ListView(
                scrollDirection: Axis.vertical,
                children: [
                 Container(
                   decoration: BoxDecoration(color: Colors.grey[350],borderRadius: BorderRadius.circular(15)),
                   height: 80,
                   margin: EdgeInsets.all(8),
                   child: ListTile(
                    title: Text('Enrollment Date',style: TextStyle(fontSize: 24),),
                     trailing: Icon(Icons.edit_calendar_outlined,size: 60,),
                   ),
                 ),Container(
                    decoration: BoxDecoration(color: Colors.grey[350],borderRadius: BorderRadius.circular(15)),
                    height: 80,
                    margin: EdgeInsets.all(8),
                   child: ListTile(
                     title: Text('Graduation Request',style: TextStyle(fontSize: 24),),
                     trailing: Icon(Icons.school_outlined,size: 60,),
                   ),
                 ),Container(
                    decoration: BoxDecoration(color: Colors.grey[350],borderRadius: BorderRadius.circular(15)),
                    height: 80,
                    margin: EdgeInsets.all(8),
                   child: ListTile(
                    title: Text('Payment Schedule (Second Semester, 2022)',style: TextStyle(fontSize: 24)),
                     trailing: Icon(Icons.payments_outlined,size: 60,),
                   ),
                 ),Container(
                    decoration: BoxDecoration(color: Colors.grey[350],borderRadius: BorderRadius.circular(15)),
                    height: 80,
                    margin: EdgeInsets.all(8),
                   child: ListTile(
                    title: Text('Krung Thai VISA Debit Card for Students',style: TextStyle(fontSize: 24)),
                     trailing: Icon(Icons.credit_card,size: 60,),
                   ),
                 ),Container(
                    decoration: BoxDecoration(color: Colors.grey[350],borderRadius: BorderRadius.circular(15)),
                    height: 80,
                    margin: EdgeInsets.all(8),
                   child: ListTile(
                    title: Text('Reimbursement documents',style: TextStyle(fontSize: 24)),
                     trailing: Icon(Icons.note,size: 60,),
                   ),
                 ),

                ],
              )
            )
          ],
        ),
      ),
    );
  }}



class MyNavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Drawer(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buildHeader(context),
              buildMenuItems(context),
            ],
          ),
        ),
      );
  Widget buildHeader(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Container(
          color: Color(0xffffd700),
          padding: EdgeInsets.only(
            top: 24 + MediaQuery.of(context).padding.top,
            bottom: 24,
          ),
          child: Column(
            children: [

              CircleAvatar(
                  radius: 60,
                  backgroundImage: AssetImage('assets/getstudentimage.jpg')),
              SizedBox(
                height: 12,
              ),
              Text(
                'Nontapat Wisesvongsa',
                style: TextStyle(fontSize: 28),
              ),
              Text(
                '63160077@go.buu.ac.th',
                style: TextStyle(fontSize: 16),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildMenuItems(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: Icon(Icons.home_outlined),
          title: Text("Home"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => Home()));
          },
        ),
        ListTile(
          leading: Icon(Icons.edit_calendar_outlined),
          title: Text("Enroll"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => EnrollPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.fact_check_outlined),
          title: Text("Enrollment Result"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => EnrollResultPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.approval_outlined),
          title: Text("Approve Add-Drop"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => AprrovePage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.event_note_outlined),
          title: Text("Study/Exam Timetable"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => TimeTablePage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.account_circle),
          title: Text("Bibliography"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => BibliographyPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.monetization_on_outlined),
          title: Text("Dept/Scholarship"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => DeptScholarPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.analytics_outlined),
          title: Text("Studied result"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => StudyResultPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.school_outlined),
          title: Text("Graduation Check"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => GraduatePage()));
          },
        ),
        Divider(color: Colors.black54),
        ListTile(
          leading: Icon(Icons.message_outlined),
          title: Text("Suggest"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => SuggestPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.people_alt_outlined),
          title: Text("STUDENT'S LIST"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => StudentListPage()));
          },
        ),
        ListTile(
          leading: Icon(Icons.history),
          title: Text("Weblog"),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => WebLogPage()));
          },
        ),
        ListTile(
          iconColor: Colors.red,
          leading: Icon(
            Icons.price_check_outlined,
          ),
          title: Text(
            "Check Debt",
            style: TextStyle(color: Colors.red),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => CheckDebtPage()));
          },
        ),
      ],
    );
  }
}
