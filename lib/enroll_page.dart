import 'package:flutter/material.dart';
import 'package:mobile_midterm_work/main.dart';

class EnrollPage extends StatelessWidget {
  const EnrollPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyNavigationDrawer(),
      appBar: AppBar(
        title: Text('Enroll'),
      ),
      body: Container(
          child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon((Icons.construction), size: 80,),
                  Text("This Page is not available yet.",style: TextStyle(fontSize: 28),)
                ],
              )
          )

      ),
    );
  }

}