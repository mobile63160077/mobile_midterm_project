import 'dart:async';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const Home(),
    );
  }
}
class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late final PageController pageController;
  int pageNo  = 0;
  Timer? carouselTimer;
  Timer getTimer(){
    return Timer.periodic(Duration(seconds: 2), (timer) {
      if(pageNo == 5){
        pageNo = 0;
      }
      pageController.animateToPage(pageNo, duration: Duration(seconds: 2), curve: Curves.easeInOutCirc);
      pageNo++;
    });
  }

  @override
  void initState() {
    pageController = PageController(
        initialPage: 0,
        viewportFraction: 0.85
    );
    carouselTimer = getTimer();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(
              height: 200,
              child: PageView.builder(
                controller: pageController,
                onPageChanged: (index){
                  pageNo = index;
                  setState(() {
                  });
                },
                itemBuilder: (_, index) {
                  return AnimatedBuilder(
                    animation: pageController,
                    builder: (ctx, child) {
                      return child!;
                    },
                    child: GestureDetector(
                      onTap: (){
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Hello you tapped")));

                      },
                      onPanDown: (d){
                        carouselTimer?.cancel();
                        carouselTimer = null;
                      },
                      onPanCancel: (){
                        carouselTimer = getTimer();
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 8,left: 8,top: 36, bottom: 12),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(24),
                            color: Colors.grey
                        ),

                      ),
                    ),
                  );
                },
                itemCount: 5,
              ),
            ),
            SizedBox(height: 12,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(5, (index) => GestureDetector(

                child: Container(
                  margin: EdgeInsets.all(2),
                  child: Icon(
                    Icons.circle,
                    size: 12,
                    color: pageNo == index? Color(0xffffd700): Colors.grey,
                  ),),
              )),)

          ],
        ),
      ),
    );
  }

}